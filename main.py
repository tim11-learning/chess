from src.Board import Board
from src.const import WHITE
import re


def get_char_from_col(col: int) -> str:
    return chr(65 + col)


def get_col_from_char(char: str) -> int:
    return ord(char) - 65


def print_board(board: Board):  # Распечатать доску в текстовом виде (см. скриншот)
    print('     ' + '+---' * 8 + '+')
    for row in range(7, -1, -1):
        print(' ', row + 1, end='  ')
        for col in range(8):
            print('|', board.cell(row, col), end=' ')
        print('|')
        print('     ' + '+---' * 8 + '+')
    print(end='        ')
    for col in range(8):
        print(get_char_from_col(col), end='   ')
    print()


def main():
    # Создаём шахматную доску
    board = Board()
    regexp = r'^move ([A-H])([1-8]) ([A-H])([1-8])$'
    # Цикл ввода команд игроков
    while True:
        # Выводим положение фигур на доске
        print_board(board)
        # Подсказка по командам
        print('Команды:')
        print('    exit                               -- выход')
        print('    move <row> <col> <row1> <col1>     -- ход из клетки (row, col)')
        print('                                          в клетку (row1, col1)')
        # Выводим приглашение игроку нужного цвета
        if board.color == WHITE:
            print('Ход белых:')
        else:
            print('Ход черных:')
        command = input()
        if command == 'exit':
            break
        find = re.match(regexp, command.strip())
        if not find:
            continue
        row = int(find.group(2)) - 1
        col = get_col_from_char(find.group(1))
        row1 = int(find.group(4)) - 1
        col1 = get_col_from_char(find.group(3))
        if board.move_piece(row, col, row1, col1):
            print('Ход успешен')
        else:
            print('Координаты некорректы! Попробуйте другой ход!')


if __name__ == '__main__':
    main()
