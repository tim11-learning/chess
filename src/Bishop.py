from .BaseFigure import BaseFigure


class Bishop(BaseFigure):
    black_symb: str = '\u265D'
    white_symb: str = '\u2657'