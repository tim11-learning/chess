from .BaseFigure import BaseFigure


class Rook(BaseFigure):
    black_symb: str = '\u265C'
    white_symb: str = '\u2656'

    def can_move(self, board, row: int, col: int, row1: int, col1: int) -> bool:
        if not super().can_move(board, row, col, row1, col1):
            return False
        if row != row1 and col != col1:
            return False
        step = 1 if (row1 >= row) else -1
        for r in range(row + step, row1, step):
            if board.get_piece(r, col) is not None:
                return False
        return True
