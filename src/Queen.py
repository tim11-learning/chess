from .BaseFigure import BaseFigure


class Queen(BaseFigure):
    black_symb: str = '\u265B'
    white_symb: str = '\u2655'