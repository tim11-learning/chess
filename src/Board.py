from .BaseFigure import BaseFigure
from .Pawn import Pawn
from .Rook import Rook
from .Knight import Knight
from .Bishop import Bishop
from .Queen import Queen
from .King import King
from .const import BLACK, WHITE


class Board:
    def __init__(self):
        self.__field = list()
        self.__color = WHITE
        for row in range(8):
            self.field.append([None] * 8)
        for col in range(8):
            self.__field[1][col] = Pawn(WHITE)
            self.__field[6][col] = Pawn(BLACK)
        for row in [0, 7]:
            color = WHITE if row == 0 else BLACK
            self.__field[row][0] = Rook(color)
            self.__field[row][7] = Rook(color)
            self.__field[row][1] = Knight(color)
            self.__field[row][6] = Knight(color)
            self.__field[row][2] = Bishop(color)
            self.__field[row][5] = Bishop(color)
            self.__field[row][3] = Queen(color)
            self.__field[row][4] = King(color)

    @property
    def color(self) -> int:
        return self.__color

    @staticmethod
    def is_correct_coord(row: int, col: int) -> bool:
        return 0 <= row < 8 and 0 <= col < 8

    @property
    def field(self) -> list:
        return self.__field

    def get_piece(self, row: int, col: int) -> BaseFigure | None:
        if not self.is_correct_coord(row, col):
            return None
        return self.field[row][col]

    def cell(self, row: int, col: int) -> str:
        piece = self.get_piece(row, col)
        if piece is None:
            return ' '
        return piece.char

    def move_piece(self, row, col, row1, col1) -> bool:
        if not self.is_correct_coord(row, col) or not self.is_correct_coord(row1, col1):
            return False
        if row == row1 and col == col1:
            return False
        piece = self.get_piece(row, col)
        if piece is None:
            return False
        if piece.color != self.color:
            return False
        other_piece = self.get_piece(row1, col1)
        if not other_piece:
            if not piece.can_move(self, row, col, row1, col1):
                return False
        elif other_piece.color != piece.color:
            if not piece.can_attack(self, row, col, row1, col1):
                return False
        else:
            return False
        self.field[row][col] = None
        self.field[row1][col1] = piece
        self.__color = WHITE if self.color == BLACK else BLACK
        return True
