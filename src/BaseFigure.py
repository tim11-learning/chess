from .const import BLACK, WHITE


class BaseFigure:
    black_symb: str = ' '
    white_symb: str = ' '

    def __init__(self, color: int):
        self.__color = color

    @property
    def char(self) -> str:
        return self.black_symb if self.color == BLACK else self.white_symb

    @property
    def color(self) -> int:
        return self.__color

    def can_move(self, board, row: int, col: int, row1: int, col1: int) -> bool:
        return board.is_correct_coord(row, col) and board.is_correct_coord(row1, col1)

    def can_attack(self, board, row: int, col: int, row1: int, col1: int) -> bool:
        return self.can_move(board, row, col, row1, col1)
