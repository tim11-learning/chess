from .const import WHITE
from .BaseFigure import BaseFigure


class Pawn(BaseFigure):
    black_symb: str = '\u265F'
    white_symb: str = '\u2659'

    def can_move(self, board, row: int, col: int, row1: int, col1: int) -> bool:
        if not super().can_move(board, row, col, row1, col1):
            return False
        if col != col1:
            return False
        if self.color == WHITE:
            direction = 1
            start_row = 1
        else:
            direction = -1
            start_row = 6
        if row + direction == row1:
            return True
        other_piece = board.get_piece(row + direction, col)
        if row == start_row and row + 2 * direction == row1 and not other_piece:
            return True
        return False

    def can_attack(self, board, row: int, col: int, row1: int, col1: int) -> bool:
        direction = 1 if self.color == WHITE else -1
        return row + direction == row1 and (col + 1 == col1 or col - 1 == col1)
