from .BaseFigure import BaseFigure


class King(BaseFigure):
    black_symb: str = '\u265A'
    white_symb: str = '\u2654'