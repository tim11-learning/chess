from .BaseFigure import BaseFigure


class Knight(BaseFigure):
    black_symb: str = '\u265E'
    white_symb: str = '\u2658'

    def can_move(self, board, row: int, col: int, row1: int, col1: int) -> bool:
        if not super().can_move(board, row, col, row1, col1):
            return False
        diff_col = abs(col - col1)
        diff_row = abs(row1 - row)
        return diff_col == 1 and diff_row == 2 or diff_col == 2 and diff_row == 1
